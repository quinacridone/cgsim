import adapter from "@sveltejs/adapter-static";
import preprocess from "svelte-preprocess";

const isProd = process.env.IS_PROD;
/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter({
			pages: 'public',
			assets: 'public',
			fallback: 'index.html'
		}),
		paths: {
			base: isProd ? '/cgsim' : '',
		},
		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte'
	}
};

export default config;
