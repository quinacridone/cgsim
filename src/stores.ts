import { writable } from "svelte/store";

export const balance = writable(300);
export const totalExpenses = writable(0);
export const totalBadgeCapacity = writable(0);
export const totalWillpower = writable(0);
export const usableWillpower = writable(0);
export const rewards = writable(0);
export const mintedSentz = writable([]);
export const mintedBadge = writable([]);
export const popupVisible = writable(false);
export const popupMessage = writable("");
export const popupIcon = writable("");
export const selectedLevel = writable(1);
