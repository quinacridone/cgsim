export const SENTZ_COST = 30;
export const LEGION_BADGE_COST = 20;

export const SENTZ_WEIGHTS = {
	1: 0.41,
	2: 0.25,
	3: 0.15,
	4: 0.08,
	5: 0.06,
	6: 0.03,
	7: 0.02
};

export const SENTZ_RARITIES = [
	{
		rarity: 1,
		name: 'Ancient',
		willpower: {
			min: 50,
			max: 95
		}
	},
	{
		rarity: 2,
		name: 'Awakened',
		willpower: {
			min: 96,
			max: 141
		}
	},
	{
		rarity: 3,
		name: 'Hermit',
		willpower: {
			min: 142,
			max: 187
		}
	},
	{
		rarity: 4,
		name: 'Mystic',
		willpower: {
			min: 188,
			max: 233
		}
	},
	{
		rarity: 5,
		name: 'Ether',
		willpower: {
			min: 234,
			max: 279
		}
	},
	{
		rarity: 6,
		name: 'DemiGod',
		willpower: {
			min: 280,
			max: 323
		}
	},
	{
		rarity: 7,
		name: 'Immortal',
		willpower: {
			min: 324,
			max: 369
		}
	}
];

export const LEGION_BADGE_WEIGHTS = {
	1: 0.45,
	2: 0.35,
	3: 0.12,
	4: 0.05,
	5: 0.03
};

export const LEGION_BADGE = [
	{
		rarity: 1,
		capacity: 1
	},
	{
		rarity: 2,
		capacity: 2
	},
	{
		rarity: 3,
		capacity: 3
	},
	{
		rarity: 4,
		capacity: 4
	},
	{
		rarity: 5,
		capacity: 5
	}
];

export const ELAN_VITAL = [
	{
		duration: 7,
		costPerSentz: 7
	},
	{
		duration: 15,
		costPerSentz: 12
	},
	{
		duration: 30,
		costPerSentz: 22
	}
];

export const REWARDS_WILLPOWER = [
	150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500, 1650, 1800, 1950, 2100, 2250, 2400, 2550,
	2700, 2850, 3000, 3150, 3300, 3450, 3600, 3750, 3900, 4050, 4200, 4350, 4500
];
export const REWARDS_MAX_WINRATE = { 0: 0.1, 1: 0.9 };
export const REWARDS_WP_MAX_WINRATE = [
	3050, 3050, 3050, 3050, 3050, 3550, 3550, 3550, 3550, 3550, 4050, 4050, 4050, 4550, 4550, 4550,
	4550, 4700, 5350, 5500, 5650, 5800, 5950, 6100, 6250, 6400, 6550, 6700, 6850, 7000
];

export const REWARDS_AMOUNT = [
	4.89, 9.78, 14.67, 20.18, 25.68, 31.79, 37.9, 44.02, 50.13, 56.86, 70.31, 78.87, 88.04, 97.21,
	109.44, 117.99, 129.61, 141.23, 153.46, 166.29, 217.65, 237.22, 258, 280.01, 303.25, 328.31,
	354.6, 382.73, 412.07, 443.87
];

export const REWARDS_WINRATE = [
	{
		0: 0.12,
		1: 0.88
	},
	{
		0: 0.14,
		1: 0.86
	},
	{
		0: 0.16,
		1: 0.84
	},
	{
		0: 0.16,
		1: 0.82
	},
	{
		0: 0.2,
		1: 0.8
	},
	{
		0: 0.22,
		1: 0.78
	},
	{
		0: 0.24,
		1: 0.76
	},
	{
		0: 0.26,
		1: 0.74
	},
	{
		0: 0.28,
		1: 0.72
	},
	{
		0: 0.3,
		1: 0.7
	},
	{
		0: 0.35,
		1: 0.65
	},
	{
		0: 0.37,
		1: 0.63
	},
	{
		0: 0.39,
		1: 0.61
	},
	{
		0: 0.41,
		1: 0.59
	},
	{
		0: 0.43,
		1: 0.57
	},
	{
		0: 0.45,
		1: 0.55
	},
	{
		0: 0.47,
		1: 0.53
	},
	{
		0: 0.49,
		1: 0.51
	},
	{
		0: 0.51,
		1: 0.49
	},
	{
		0: 0.53,
		1: 0.47
	},
	{
		0: 0.57,
		1: 0.43
	},
	{
		0: 0.57,
		1: 0.43
	},
	{
		0: 0.57,
		1: 0.43
	},
	{
		0: 0.57,
		1: 0.43
	},
	{
		0: 0.57,
		1: 0.43
	},
	{
		0: 0.6,
		1: 0.4
	},
	{
		0: 0.6,
		1: 0.4
	},
	{
		0: 0.6,
		1: 0.4
	},
	{
		0: 0.6,
		1: 0.4
	},
	{
		0: 0.6,
		1: 0.4
	}
];
